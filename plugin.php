<?php
/**
 * Plugin Name: WP Nav Menu Block
 * Plugin URI: https://github.com/ahmadawais/create-guten-block/
 * Description: Implements a new Block that outputs a WordPress custom navigation menu
 * Author: cgrymala
 * Author URI: https://umw.edu/
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package UMW
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
