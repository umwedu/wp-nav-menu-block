<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace UMW\WP_Nav_Menu {

	class Block {
		/**
		 * @var Block $instance holds the single instance of this class
		 * @access private
		 */
		private static $instance;
		/**
		 * @var string $version holds the version number for the plugin
		 * @access public
		 */
		public static $version = '2020.7.1.1';

		/**
		 * Creates the Block object
		 *
		 * @access private
		 * @since  0.1
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'block_assets' ) );
		}

		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @return  Block
		 * @since   0.1
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className      = __CLASS__;
				self::$instance = new $className;
			}

			return self::$instance;
		}

		/**
		 * Custom logging function that can be short-circuited
		 *
		 * @access public
		 * @return void
		 * @since  0.1
		 */
		public static function log( $message ) {
			if ( ! defined( 'WP_DEBUG' ) || false === WP_DEBUG ) {
				return;
			}

			error_log( '[UMW WP Nav Menu Block]: ' . $message );
		}

		/**
		 * Retrieve a URL relative to the root of this plugin
		 *
		 * @param string $path the path to append to the root plugin path
		 *
		 * @access public
		 * @return string the full URL to the provided path
		 * @since  0.1
		 */
		public static function plugins_url( $path ) {
			return plugins_url( $path, dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
		}

		/**
		 * Retrieve and return the root path of this plugin
		 *
		 * @access public
		 * @return string the absolute path to the root of this plugin
		 * @since  0.1
		 */
		public static function plugin_dir_path() {
			return plugin_dir_path( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
		}

		/**
		 * Retrieve and return the root URL of this plugin
		 *
		 * @access public
		 * @return string the absolute URL
		 * @since  0.1
		 */
		public static function plugin_dir_url() {
			return plugin_dir_url( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
		}

		/**
		 * Set up the block assets
		 *
		 * @access public
		 * @return void
		 * @since  0.1
		 */
		public function block_assets() {
			// Register block styles for both frontend + backend.
			wp_register_style(
				'wp_nav_menu_block-cgb-style-css', // Handle.
				self::plugins_url( 'dist/blocks.style.build.css' ), // Block style CSS.
				is_admin() ? array( 'wp-editor' ) : null, // Dependency to include the CSS after it.
				null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
			);

			// Register block editor script for backend.
			wp_register_script(
				'wp_nav_menu_block-cgb-block-js', // Handle.
				self::plugins_url( '/dist/blocks.build.js' ), // Block.build.js: We register the block here. Built with Webpack.
				array(
					'wp-blocks',
					'wp-i18n',
					'wp-element',
					'wp-editor',
					'wp-components',
					'wp-compose',
				), // Dependencies, defined above.
				null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
				true // Enqueue the script in the footer.
			);

			// Register block editor styles for backend.
			wp_register_style(
				'wp_nav_menu_block-cgb-block-editor-css', // Handle.
				self::plugins_url( 'dist/blocks.editor.build.css' ), // Block editor CSS.
				array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
				null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
			);

			// WP Localized globals. Use dynamic PHP stuff in JavaScript via `cgbGlobal` object.
			wp_localize_script(
				'wp_nav_menu_block-cgb-block-js',
				'cgbGlobal', // Array containing dynamic data for a JS Global.
				[
					'pluginDirPath' => self::plugin_dir_path(),
					'pluginDirUrl'  => self::plugin_dir_url(),
					'navMenuList'   => $this->get_options(),
					'restURL'       => get_rest_url( $GLOBALS['blog_id'], '/umw/v1/nav-menu/' ),
					// Add more data here that you want to access from `cgbGlobal` object.
				]
			);

			/**
			 * Register Gutenberg block on server-side.
			 *
			 * Register the block on server-side to ensure that the block
			 * scripts and styles for both frontend and backend are
			 * enqueued when the editor loads.
			 *
			 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
			 * @since 1.16.0
			 */
			register_block_type(
				'umw/block-wp-nav-menu-block', array(
					// Enqueue blocks.style.build.css on both frontend & backend.
					'style'           => 'wp_nav_menu_block-cgb-style-css',
					// Enqueue blocks.build.js in the editor only.
					'editor_script'   => 'wp_nav_menu_block-cgb-block-js',
					// Enqueue blocks.editor.build.css in the editor only.
					'editor_style'    => 'wp_nav_menu_block-cgb-block-editor-css',
					'attributes' => array(
						'menu_id' => array(
							'type' => 'object',
						),
					),
					'render_callback' => array( $this, 'render' ),
				)
			);
		}

		/**
		 * Render the WP Nav Menu as a block
		 *
		 * @param array $atts the block attributes
		 * @param string $content the content of the block
		 *
		 * @access public
		 * @return string
		 * @since  0.1
		 */
		function render( array $atts, $content = '' ) {
			if ( ! is_array( $atts ) || ! array_key_exists( 'menu_id', $atts ) ) {
				return '';
			}
			if ( ! array_key_exists( 'className', $atts ) ) {
				$atts['className'] = '';
			}

			ob_start();
			echo '<div class="umw-wp-nav-menu-block ' . $atts['className'] . '">';
			wp_nav_menu( array(
				'menu' => $atts['menu_id']['key'],
			) );
			echo '</div>';

			return ob_get_clean();
		}

		/**
		 * Retrieve and return a list of the registered navigation menus
		 *
		 * @access public
		 * @return array the list of registered nav menus
		 * @since  0.1
		 */
		public function get_options() {
			$list = wp_get_nav_menus();
			$rt   = array();
			foreach ( $list as $item ) {
				$rt[] = array(
					'key'  => $item->term_id,
					'name' => $item->name,
				);
			}

			return $rt;
		}
	}
}
