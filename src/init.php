<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

include_once plugin_dir_path( __DIR__ ) . 'src/classes/umw/wp-nav-menu/block.php';

\UMW\WP_Nav_Menu\Block::instance();
