/**
 * BLOCK: wp-nav-menu-block
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { ComboboxControl, CustomSelectControl } = wp.components;
const { useState } = wp.element;
const { ServerSideRender } = wp.editor;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'umw/block-wp-nav-menu-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'WP Nav Menu Block' ), // Block title.
	icon: 'menu', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'WP Nav Menu Block' ),
		__( 'Menu' ),
		__( 'navigation' ),
	],
	attributes: {
		menu_id: {
			type: 'object',
		},
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 *
	 * @param {Object} props Props.
	 * @returns {Mixed} JSX Component.
	 */
	edit: ( props ) => {
		const options = cgbGlobal.navMenuList;
		if (options[0].key !== 0) {
			options.unshift( {
				key: 0,
				name: '-- Please select a menu --',
			} );
		}

		const {
			className,
			isSelected,
			attributes: { menu_id },
			setAttributes,
		} = props;

		function getComboBoxControl() {
			let selected = options[0];

			if (typeof menu_id !== 'undefined') {
				selected = menu_id;
			}

			const [ fontSize, setFontSize ] = useState( selected );

			return (
				<CustomSelectControl
					label="Navigation Menu"
					options={ options }
					onChange={ ( newValue, props ) => {
						setAttributes( { menu_id: newValue.selectedItem } );
						return setFontSize( newValue );
					} }
					value={ options.find( ( option ) => option.key === fontSize.key ) }
				/>
			);
		}

		function getSampleNavMenu( props ) {
			let selected = options[0];

			if (typeof menu_id !== 'undefined') {
				selected = menu_id;
			}

			if (selected.key === 0) {
				return (
					<div className="menu-sidebar-nav-container">
						<ul className="menu">
							<li className="menu-item">
								<a href="#">Sample Link 1</a></li>
							<li className="menu-item">
								<a href="#">Sample Link 2</a></li>
							<li className="menu-item">
								<a href="#">Sample Link 3</a></li>
							<li className="menu-item">
								<a href="#">&hellip;</a></li>
						</ul>
					</div>
				);
			} else {
				return (
					<ServerSideRender
						block="umw/block-wp-nav-menu-block"
						attributes={ { menu_id: selected } }
					/>
				);
			}
		}

		// Creates a <p class='wp-block-cgb-block-wp-nav-menu-block'></p>.
		return (
			<div className={ className }>
				{ ! isSelected && getSampleNavMenu( props ) }
				{ isSelected && getComboBoxControl() }
			</div>
		);
	},
} );
